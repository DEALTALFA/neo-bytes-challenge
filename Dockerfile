FROM centos

WORKDIR /mnt

COPY my_first_script.sh .

RUN chmod +x my_first_script.sh

CMD ["./my_first_script.sh"]
